#!/bin/bash

echo "Ingrese el código alfanumérico de la proteina que desea descargar"
read codigo

wget https://icb.utalca.cl/~fduran/bd-pdb.txt

numerodelineas=`cat bd-pbd.txt | wc -l`

for i in `seq 1 $numerodelineas`
do
   proteina=grep '"code"$'| awk -F"," '{print substr($1,2,4)} '
   
   if [ "$codigo" = "$proteina" ]; then
		echo "el codigo ingresado corresponde a una proteina, puede seguir con el graficador"
		
	elif [ "$codigo" != "$proteina" ]; then
   
done


bash descargador.sh $codigo
bash modificador.sh $codigo

dot -Tpng -o $codigo.png $codigo.dot
