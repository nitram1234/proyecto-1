El objetivo del proyecto es que este pueda recibir un codigo alfanumerico correspondiente al identificador de una biomolecula perteneciente a la base de datos PDB.
Ademas este programa debe reconocer si el identificador ingresado corresponde a una proteina o no, puesto que su fin es graficar proteinas especificamente.
Para esto se nos entrega una lista de proteinas de PDB con la cual se debe comparar el codigo ingresado al programa.
Luego el codigo debe ser capaz de discriminar entre la informacion necesaria para graficar la proteina y la informacion que no nos interesa.
Finalmente esta informacion debe ser organizada, a traves de un archivo.awk, correctamente en un archivo.dot para finalmente ser procesada por el programa graphviz.